
// Vulkan uses plain "enum" vs. "enum class", and it's
// not worth overhauling the entire Vulkan header file.

#pragma warning(push)
#pragma warning(disable: 26812)
#define GLFW_INCLUDE_VULKAN
#include <GLFW/glfw3.h>
#pragma warning(pop)

#define GLM_FORCE_RADIANS
#define GLM_FORCE_DEPTH_ZERO_TO_ONE
#include <glm/vec4.hpp>
#include <glm/mat4x4.hpp>

#include <iostream>
#include <stdexcept>
#include <functional>
#include <cstdlib>
#include <vector>
#include <optional>


// Constants.
const int WIDTH  = 800;
const int HEIGHT = 600;


#ifdef NDEBUG
const bool enable_validation_layers = false;
#else
const bool enable_validation_layers = true;
#endif

// TODO: Not a fan of "optional", but following the tutorial for now.
struct QueueFamilyIndices
{
    std::optional<uint32_t> graphics_family;
    std::optional<uint32_t> present_family;

    bool isComplete() {
        bool result = graphics_family.has_value();
        // && present_family.has_value(); TODO: WHY DO WE NOT FIND PRESENT FAMILY?
        return result;
    }
};

class HelloTriangleApp {
public:
    HelloTriangleApp();
    bool run();

private:
    GLFWwindow *m_window;
    VkInstance  m_instance;
    VkDebugUtilsMessengerEXT m_debug_messenger;
    VkPhysicalDevice m_physical_device;
    QueueFamilyIndices m_queue_family_indices;
    VkDevice m_logical_device;
    VkQueue m_graphics_queue;
    VkSurfaceKHR m_surface;

    bool runBody();
    bool initVulkan();
    bool initDebugMessenger();
    bool createSurface();
    bool pickPhysicalDevice();
    bool isPhysicalDeviceSuitable(const VkPhysicalDevice& device);
    QueueFamilyIndices findQueueFamilies(const VkPhysicalDevice& device);
    bool createLogicalDevice();
    void cleanup();
};


// Stand-alone functions.
std::vector<const char *> GetValidationLayersVec();

VKAPI_ATTR VkBool32 VKAPI_CALL MyDebugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT      msg_severity,
    VkDebugUtilsMessageTypeFlagsEXT             msg_type,
    const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
    void* user_data);


// Constructor.
HelloTriangleApp::HelloTriangleApp()
{
    m_window   = nullptr;
    m_instance = nullptr;
    m_debug_messenger = VK_NULL_HANDLE;
    m_physical_device = VK_NULL_HANDLE;
    m_logical_device  = VK_NULL_HANDLE;
    m_graphics_queue  = VK_NULL_HANDLE;
    m_surface = VK_NULL_HANDLE;
}


// Run the app. No matter what happens, perform cleanup at the end.
bool HelloTriangleApp::run()
{
    bool result = runBody();
    cleanup();
    return result;
}

bool HelloTriangleApp::runBody()
{
    // Create the GLFW window. Not much to it.
    glfwInit();
    glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
    m_window = glfwCreateWindow(WIDTH, HEIGHT, "Vulkan window", nullptr, nullptr);

    if (!initVulkan()) {
        return false;
    }

    if (!initDebugMessenger()) {
        return false;
    }

    if (!createSurface()) {
        return false;
    }

    if (!pickPhysicalDevice()) {
        return false;
    }

    if (!createLogicalDevice()) {
        return false;
    }

    // The main GLFW event loop.
    while (!glfwWindowShouldClose(m_window)) {
        glfwPollEvents();
    }

    return true;
}


bool HelloTriangleApp::initVulkan()
{
    // First, make sure our expected validation layers are installed.
    std::vector<const char *> val_layer_names;
    if (enable_validation_layers) {
        val_layer_names = GetValidationLayersVec();
        if (val_layer_names.empty()) {
            printf("ALAN: Validation layers requested, but not available!");
            return false;
        }
    }

    // Print out what extensions are available.
    uint32_t extn_count = 0;
    vkEnumerateInstanceExtensionProperties(nullptr, &extn_count, nullptr);

    std::vector<VkExtensionProperties> extn_props(extn_count);
    vkEnumerateInstanceExtensionProperties(nullptr, &extn_count, extn_props.data());

    printf("ALAN: Vulkan extensions...\n");
    for (const auto iter : extn_props) {
        printf("    %s (%u)\n", iter.extensionName, iter.specVersion);
    }
    printf("\n");

    // Get our GLFW extentions.
    uint32_t glfw_extn_count = 0;
    const char** glfw_extns_raw = glfwGetRequiredInstanceExtensions(&glfw_extn_count);

    std::vector<const char*> glfw_extns(glfw_extns_raw, glfw_extns_raw + glfw_extn_count);
    if (enable_validation_layers) {
        glfw_extns.push_back(VK_EXT_DEBUG_UTILS_EXTENSION_NAME);
        glfw_extns.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
    }

    printf("ALAN: GLFW extensions...\n");
    for (const auto &iter : glfw_extns) {
        printf("    %s\n", iter);
    }
    printf("\n");

    // Debug info for the create call.
    VkDebugUtilsMessengerCreateInfoEXT debug_create_info = {};
    
    debug_create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    debug_create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT |
                                        VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT |
                                        VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    debug_create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT |
                                    VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT |
                                    VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    debug_create_info.pfnUserCallback = MyDebugCallback;

    // App info for the create call.
    VkApplicationInfo app_info = {};
    
    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pApplicationName   = "Alan Vulkan FPS";
    app_info.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
    app_info.pEngineName        = "Unnamed Engine";
    app_info.engineVersion      = VK_MAKE_VERSION(1, 0, 0);
    app_info.apiVersion         = VK_API_VERSION_1_0;

    // Put it all together.
    VkInstanceCreateInfo create_info = {};

    create_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    create_info.pApplicationInfo        = &app_info;
    create_info.enabledExtensionCount   = static_cast<uint32_t>(glfw_extns.size());
    create_info.ppEnabledExtensionNames = glfw_extns.data();
    create_info.enabledLayerCount       = static_cast<uint32_t>(val_layer_names.size());
    create_info.ppEnabledLayerNames     = val_layer_names.data();

    if (enable_validation_layers) {
        create_info.pNext = &debug_create_info;
    }
    else {
        create_info.pNext = nullptr;
    }

    VkResult result = vkCreateInstance(&create_info, nullptr, &m_instance);
    if (result != VK_SUCCESS) {
        printf("ALAN: Failed to create instance: %d\n", result);
        return false;
    }

    printf("ALAN: Woo! Successfully created instance.\n");
    return true;
}


void HelloTriangleApp::cleanup()
{
    if (m_debug_messenger != 0) {
        // You have to look up the address of this function on the fly.
        auto func = (PFN_vkDestroyDebugUtilsMessengerEXT)vkGetInstanceProcAddr(m_instance, "vkDestroyDebugUtilsMessengerEXT");
        if (func != nullptr) {
            func(m_instance, m_debug_messenger, nullptr);
            m_debug_messenger = VK_NULL_HANDLE;
        }
    }

    // Destroy the logical device.
    if (m_logical_device != VK_NULL_HANDLE) {
        vkDestroyDevice(m_logical_device, nullptr);
        m_logical_device = VK_NULL_HANDLE;
    }

    // Destroy the surface.
    if ((m_surface != VK_NULL_HANDLE) && (m_instance != nullptr)) {
        vkDestroySurfaceKHR(m_instance, m_surface, nullptr);
        m_surface = VK_NULL_HANDLE;
    }

    // Destroying the instance implicitly destroys the physical
    // device as well, so there's no need to do that separately.
    if (m_instance != nullptr) {
        vkDestroyInstance(m_instance, nullptr);
        m_instance = nullptr;
    }

    if (m_window != nullptr) {
        glfwDestroyWindow(m_window);
        m_window = nullptr;
    }

    glfwTerminate();
}


// Go through all the available validation layers, and make sure the ones we
// require are available. Return just the ones we require. Forget the rest.
std::vector<const char *> GetValidationLayersVec()
{
    std::vector<const char*> required_layers;
    required_layers.push_back("VK_LAYER_KHRONOS_validation");

    // Get a list of all the installed validation layers.
    uint32_t layer_count = 0;
    vkEnumerateInstanceLayerProperties(&layer_count, nullptr);

    std::vector<VkLayerProperties> layer_props(layer_count);
    vkEnumerateInstanceLayerProperties(&layer_count, layer_props.data());

    printf("ALAN: All the available Vulkan layers...\n");
    for (const auto &iter : layer_props) {
        printf(
            "    %s, %s, %u, %u\n", 
            iter.layerName, iter.description,
            iter.implementationVersion, iter.specVersion);
    }
    printf("\n");

    // Make sure our required ones are there.
    bool found_all = true;
    for (const char* required_name : required_layers) {
        bool found_one = false;
        for (const auto &iter : layer_props) {
            if (strcmp(required_name, iter.layerName) == 0) {
                printf("ALAN: Found required layer '%s'. Good.\n", required_name);
                found_one = true;
            }
        }

        if (!found_one) {
            printf("ALAN: Could not find required validation layer '%s'!\n", required_name);
            found_all = false;
        }
    }

    printf("\n");
    if (found_all) {
        return std::move(required_layers);
    }
    else {
        std::vector<const char *> empty;
        return std::move(empty);
    }
}


// A callback function for Vulkan debugging messages.
// Note that this is a stand-alone function.
VKAPI_ATTR VkBool32 VKAPI_CALL MyDebugCallback(
    VkDebugUtilsMessageSeverityFlagBitsEXT      msg_severity,
    VkDebugUtilsMessageTypeFlagsEXT             msg_type,
    const VkDebugUtilsMessengerCallbackDataEXT* callback_data,
    void* user_data)
{
    if (msg_severity >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT) {
        printf("ALAN: Validation Layer: %s\n", callback_data->pMessage);
        return VK_FALSE;
    }

    return VK_TRUE;
}


// Set up a debug messenger.
bool HelloTriangleApp::initDebugMessenger() {
    if (!enable_validation_layers) {
        return false;
    }

    VkDebugUtilsMessengerCreateInfoEXT create_info = {};
    create_info.sType = VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT;
    create_info.messageSeverity = VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT | 
                                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT | 
                                  VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT;
    create_info.messageType = VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT | 
                              VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT | 
                              VK_DEBUG_UTILS_MESSAGE_TYPE_PERFORMANCE_BIT_EXT;
    create_info.pfnUserCallback = MyDebugCallback;
    create_info.pUserData = nullptr;

    // You have to look up the address of this function on the fly.
    auto func = (PFN_vkCreateDebugUtilsMessengerEXT) vkGetInstanceProcAddr(m_instance, "vkCreateDebugUtilsMessengerEXT");
    if (func == nullptr) {
        printf("ALAN: Could not look up function 'vkCreateDebugUtilsMessengerEXT'.\n");
        return false;
    }

    VkResult result = func(m_instance, &create_info, nullptr, &m_debug_messenger);
    if (result != VK_SUCCESS) {
        printf("ALAN: Call to '' didn't work: %d\n", result);
        return false;
    }

    return true;
}


// Create a surface.
bool HelloTriangleApp::createSurface()
{
    if (glfwCreateWindowSurface(m_instance, m_window, nullptr, &m_surface) != VK_SUCCESS) {
        printf("ALAN: failed to create window surface!\n");
        return false;
    }
    
    return true;
}


// Pick a physical device.
bool HelloTriangleApp::pickPhysicalDevice()
{
    uint32_t device_count = 0;
    vkEnumeratePhysicalDevices(m_instance, &device_count, nullptr);
    if (device_count == 0) {
        printf("ALAN: Could not find any GPUs. Huh?!\n");
        return false;
    }

    printf("ALAN: Found %u physical device(s).\n", device_count);
    std::vector<VkPhysicalDevice> devices(device_count);
    vkEnumeratePhysicalDevices(m_instance, &device_count, devices.data());

    for (const auto& iter : devices) {
        if (isPhysicalDeviceSuitable(iter)) {
            m_physical_device = iter;
            break;
        }
    }

    if (m_physical_device == VK_NULL_HANDLE) {
        printf("ALAN: Could not find a suitable GPU. Huh?!\n");
        return false;
    }

    return true;
}


// See if a particular physical device is suitable for use.
// NOTE: The tutorial mentions that we could "rank" devices and go with the 
// best one, but we don't need any of that for now. Keep it simple.
bool HelloTriangleApp::isPhysicalDeviceSuitable(const VkPhysicalDevice& device)
{
    VkPhysicalDeviceProperties device_props;
    vkGetPhysicalDeviceProperties(device, &device_props);

    VkPhysicalDeviceFeatures device_features;
    vkGetPhysicalDeviceFeatures(device, &device_features);

    printf("Device ID: %u\n", device_props.deviceID);
    printf("    Name = %s\n", device_props.deviceName);
    printf("    Type = %u\n", device_props.deviceType);

    m_queue_family_indices = findQueueFamilies(device);

    bool is_valid =
        (device_props.deviceType == VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU) &&
        device_features.geometryShader &&
        m_queue_family_indices.isComplete();

    return is_valid;
}


// Find the graphics queue family for a physical device.
QueueFamilyIndices HelloTriangleApp::findQueueFamilies(const VkPhysicalDevice& device)
{
    uint32_t family_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(device, &family_count, nullptr);

    std::vector<VkQueueFamilyProperties> family_vec(family_count);
    vkGetPhysicalDeviceQueueFamilyProperties(device, &family_count, family_vec.data());

    QueueFamilyIndices result;

    int i = 0;
    for (const auto &iter : family_vec) {
        if (iter.queueFlags & VK_QUEUE_GRAPHICS_BIT) {
            result.graphics_family = i;
            i++;
        }

        VkBool32 present_support = false;
        vkGetPhysicalDeviceSurfaceSupportKHR(device, i, m_surface, &present_support);
        if (present_support) {
            result.present_family = i;
        }
    }

    return std::move(result);
}


// Create a logical device.
bool HelloTriangleApp::createLogicalDevice()
{
    // Get our validation layers all over again. Supposedly these are ignored by
    // new implementations, but it's still a good idea to populate them anyway.
    std::vector<const char*> val_layer_names = GetValidationLayersVec();

    // Get our device features all over again.
    VkPhysicalDeviceFeatures device_features;
    vkGetPhysicalDeviceFeatures(m_physical_device, &device_features);

    // A queue priority is always required, even for just one queue.
    float queue_priority = 1.0f;

    // First, the create queue info.
    VkDeviceQueueCreateInfo queue_create_info = {};
    queue_create_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_create_info.queueFamilyIndex = m_queue_family_indices.graphics_family.value();
    queue_create_info.queueCount = 1;

    queue_create_info.pQueuePriorities = &queue_priority;

    // Then, the device itself.
    VkDeviceCreateInfo create_info = {};
    create_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    create_info.pQueueCreateInfos = &queue_create_info;
    create_info.queueCreateInfoCount = 1;
    create_info.pEnabledFeatures = &device_features;

    create_info.enabledExtensionCount = 0;

    if (enable_validation_layers) {
        create_info.enabledLayerCount = static_cast<uint32_t>(val_layer_names.size());
        create_info.ppEnabledLayerNames = val_layer_names.data();
    }
    else {
        create_info.enabledLayerCount = 0;
    }

    VkResult result = vkCreateDevice(m_physical_device, &create_info, nullptr, &m_logical_device);
    
    if (result != VK_SUCCESS) {
        printf("ALAN: Could not create logical device: %u\n", result);
        return false;
    }

    return true;
}


// And away we go.
int main(int argc, char **argv) 
{
    // Test that GLM works okay.
    glm::mat4 matrix;
    glm::vec4 vec;
    auto test = matrix * vec;

    // Run the app.
    HelloTriangleApp app;
    try {
        app.run();
    }
    catch (const std::exception &ex) {
        printf("ALAN: Exception! '%s'\n", ex.what());
        return EXIT_FAILURE;
    }

    printf("ALAN: Woo! All done.\n");
    return EXIT_SUCCESS;
}